provider "aws" {
  region = var.region
  profile = var.profile
}

locals {
  user_env_group = [
    for e in var.environment : [
      for i in var.users : {
        user_env = "${i}-${e}"
      }
    ]
  ]
user_env = flatten(local.user_env_group)
}

resource "aws_iam_user" "user" {
  for_each = {
    for j in local.user_env : j.user_env => j
  }
    name = each.key
  }

resource "aws_iam_access_key" "user" {
  for_each = {
    for j in local.user_env : j.user_env => j
  }
    user = each.key
  }

output "aws_iam_access_key" {
  value = {
    for k in aws_iam_access_key.user:
    k.id => k.secret
  }
}
