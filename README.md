# README #
# Abdul Basit #
I got this output from terraform plan...


------------------------------------------------------------------------

An execution plan has been generated and is shown below.
Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # aws_iam_user.user["breena-dev"] will be created
  + resource "aws_iam_user" "user" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "breena-dev"
      + path          = "/"
      + unique_id     = (known after apply)
    }

  # aws_iam_user.user["breena-prod"] will be created
  + resource "aws_iam_user" "user" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "breena-prod"
      + path          = "/"
      + unique_id     = (known after apply)
    }

  # aws_iam_user.user["breena-qa"] will be created
  + resource "aws_iam_user" "user" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "breena-qa"
      + path          = "/"
      + unique_id     = (known after apply)
    }

  # aws_iam_user.user["breena-test"] will be created
  + resource "aws_iam_user" "user" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "breena-test"
      + path          = "/"
      + unique_id     = (known after apply)
    }

  # aws_iam_user.user["breena-uat"] will be created
  + resource "aws_iam_user" "user" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "breena-uat"
      + path          = "/"
      + unique_id     = (known after apply)
    }

  # aws_iam_user.user["cordelia-dev"] will be created
  + resource "aws_iam_user" "user" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "cordelia-dev"
      + path          = "/"
      + unique_id     = (known after apply)
    }

  # aws_iam_user.user["cordelia-prod"] will be created
  + resource "aws_iam_user" "user" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "cordelia-prod"
      + path          = "/"
      + unique_id     = (known after apply)
    }

  # aws_iam_user.user["cordelia-qa"] will be created
  + resource "aws_iam_user" "user" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "cordelia-qa"
      + path          = "/"
      + unique_id     = (known after apply)
    }

  # aws_iam_user.user["cordelia-test"] will be created
  + resource "aws_iam_user" "user" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "cordelia-test"
      + path          = "/"
      + unique_id     = (known after apply)
    }

  # aws_iam_user.user["cordelia-uat"] will be created
  + resource "aws_iam_user" "user" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "cordelia-uat"
      + path          = "/"
      + unique_id     = (known after apply)
    }

  # aws_iam_user.user["darleen-dev"] will be created
  + resource "aws_iam_user" "user" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "darleen-dev"
      + path          = "/"
      + unique_id     = (known after apply)
    }

  # aws_iam_user.user["darleen-prod"] will be created
  + resource "aws_iam_user" "user" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "darleen-prod"
      + path          = "/"
      + unique_id     = (known after apply)
    }

  # aws_iam_user.user["darleen-qa"] will be created
  + resource "aws_iam_user" "user" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "darleen-qa"
      + path          = "/"
      + unique_id     = (known after apply)
    }

  # aws_iam_user.user["darleen-test"] will be created
  + resource "aws_iam_user" "user" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "darleen-test"
      + path          = "/"
      + unique_id     = (known after apply)
    }

  # aws_iam_user.user["darleen-uat"] will be created
  + resource "aws_iam_user" "user" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "darleen-uat"
      + path          = "/"
      + unique_id     = (known after apply)
    }

  # aws_iam_user.user["devinne-dev"] will be created
  + resource "aws_iam_user" "user" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "devinne-dev"
      + path          = "/"
      + unique_id     = (known after apply)
    }

  # aws_iam_user.user["devinne-prod"] will be created
  + resource "aws_iam_user" "user" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "devinne-prod"
      + path          = "/"
      + unique_id     = (known after apply)
    }

  # aws_iam_user.user["devinne-qa"] will be created
  + resource "aws_iam_user" "user" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "devinne-qa"
      + path          = "/"
      + unique_id     = (known after apply)
    }

  # aws_iam_user.user["devinne-test"] will be created
  + resource "aws_iam_user" "user" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "devinne-test"
      + path          = "/"
      + unique_id     = (known after apply)
    }

  # aws_iam_user.user["devinne-uat"] will be created
  + resource "aws_iam_user" "user" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "devinne-uat"
      + path          = "/"
      + unique_id     = (known after apply)
    }

  # aws_iam_user.user["emelita-dev"] will be created
  + resource "aws_iam_user" "user" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "emelita-dev"
      + path          = "/"
      + unique_id     = (known after apply)
    }

  # aws_iam_user.user["emelita-prod"] will be created
  + resource "aws_iam_user" "user" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "emelita-prod"
      + path          = "/"
      + unique_id     = (known after apply)
    }

  # aws_iam_user.user["emelita-qa"] will be created
  + resource "aws_iam_user" "user" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "emelita-qa"
      + path          = "/"
      + unique_id     = (known after apply)
    }

  # aws_iam_user.user["emelita-test"] will be created
  + resource "aws_iam_user" "user" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "emelita-test"
      + path          = "/"
      + unique_id     = (known after apply)
    }

  # aws_iam_user.user["emelita-uat"] will be created
  + resource "aws_iam_user" "user" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "emelita-uat"
      + path          = "/"
      + unique_id     = (known after apply)
    }

  # aws_iam_user.user["kriste-dev"] will be created
  + resource "aws_iam_user" "user" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "kriste-dev"
      + path          = "/"
      + unique_id     = (known after apply)
    }

  # aws_iam_user.user["kriste-prod"] will be created
  + resource "aws_iam_user" "user" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "kriste-prod"
      + path          = "/"
      + unique_id     = (known after apply)
    }

  # aws_iam_user.user["kriste-qa"] will be created
  + resource "aws_iam_user" "user" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "kriste-qa"
      + path          = "/"
      + unique_id     = (known after apply)
    }

  # aws_iam_user.user["kriste-test"] will be created
  + resource "aws_iam_user" "user" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "kriste-test"
      + path          = "/"
      + unique_id     = (known after apply)
    }

  # aws_iam_user.user["kriste-uat"] will be created
  + resource "aws_iam_user" "user" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "kriste-uat"
      + path          = "/"
      + unique_id     = (known after apply)
    }

  # aws_iam_user.user["maurita-dev"] will be created
  + resource "aws_iam_user" "user" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "maurita-dev"
      + path          = "/"
      + unique_id     = (known after apply)
    }

  # aws_iam_user.user["maurita-prod"] will be created
  + resource "aws_iam_user" "user" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "maurita-prod"
      + path          = "/"
      + unique_id     = (known after apply)
    }

  # aws_iam_user.user["maurita-qa"] will be created
  + resource "aws_iam_user" "user" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "maurita-qa"
      + path          = "/"
      + unique_id     = (known after apply)
    }

  # aws_iam_user.user["maurita-test"] will be created
  + resource "aws_iam_user" "user" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "maurita-test"
      + path          = "/"
      + unique_id     = (known after apply)
    }

  # aws_iam_user.user["maurita-uat"] will be created
  + resource "aws_iam_user" "user" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "maurita-uat"
      + path          = "/"
      + unique_id     = (known after apply)
    }

  # aws_iam_user.user["nevsa-dev"] will be created
  + resource "aws_iam_user" "user" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "nevsa-dev"
      + path          = "/"
      + unique_id     = (known after apply)
    }

  # aws_iam_user.user["nevsa-prod"] will be created
  + resource "aws_iam_user" "user" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "nevsa-prod"
      + path          = "/"
      + unique_id     = (known after apply)
    }

  # aws_iam_user.user["nevsa-qa"] will be created
  + resource "aws_iam_user" "user" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "nevsa-qa"
      + path          = "/"
      + unique_id     = (known after apply)
    }

  # aws_iam_user.user["nevsa-test"] will be created
  + resource "aws_iam_user" "user" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "nevsa-test"
      + path          = "/"
      + unique_id     = (known after apply)
    }

  # aws_iam_user.user["nevsa-uat"] will be created
  + resource "aws_iam_user" "user" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "nevsa-uat"
      + path          = "/"
      + unique_id     = (known after apply)
    }

  # aws_iam_user.user["vonnie-dev"] will be created
  + resource "aws_iam_user" "user" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "vonnie-dev"
      + path          = "/"
      + unique_id     = (known after apply)
    }

  # aws_iam_user.user["vonnie-prod"] will be created
  + resource "aws_iam_user" "user" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "vonnie-prod"
      + path          = "/"
      + unique_id     = (known after apply)
    }

  # aws_iam_user.user["vonnie-qa"] will be created
  + resource "aws_iam_user" "user" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "vonnie-qa"
      + path          = "/"
      + unique_id     = (known after apply)
    }

  # aws_iam_user.user["vonnie-test"] will be created
  + resource "aws_iam_user" "user" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "vonnie-test"
      + path          = "/"
      + unique_id     = (known after apply)
    }

  # aws_iam_user.user["vonnie-uat"] will be created
  + resource "aws_iam_user" "user" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "vonnie-uat"
      + path          = "/"
      + unique_id     = (known after apply)
    }

  # aws_iam_user.user["wynnie-dev"] will be created
  + resource "aws_iam_user" "user" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "wynnie-dev"
      + path          = "/"
      + unique_id     = (known after apply)
    }

  # aws_iam_user.user["wynnie-prod"] will be created
  + resource "aws_iam_user" "user" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "wynnie-prod"
      + path          = "/"
      + unique_id     = (known after apply)
    }

  # aws_iam_user.user["wynnie-qa"] will be created
  + resource "aws_iam_user" "user" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "wynnie-qa"
      + path          = "/"
      + unique_id     = (known after apply)
    }

  # aws_iam_user.user["wynnie-test"] will be created
  + resource "aws_iam_user" "user" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "wynnie-test"
      + path          = "/"
      + unique_id     = (known after apply)
    }

  # aws_iam_user.user["wynnie-uat"] will be created
  + resource "aws_iam_user" "user" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "wynnie-uat"
      + path          = "/"
      + unique_id     = (known after apply)
    }

Plan: 50 to add, 0 to change, 0 to destroy.

------------------------------------------------------------------------
