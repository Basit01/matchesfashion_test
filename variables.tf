variable "region" {
  default = "eu-west-2"
}

variable "profile" {
  default = "default"
}
variable "users" {
  default = (["nevsa", "cordelia", "kriste", "darleen", "wynnie", "vonnie", "emelita", "maurita", "devinne", "breena"])
}
variable "environment" {
  default = ["dev", "qa", "uat", "test", "prod"]
}


